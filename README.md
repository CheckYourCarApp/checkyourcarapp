CheckYourCarApp 
===================

Demo app for checking reasons of car failure.

# Content #

* Uses a reusable template for 'tree navigation'. The main files of this template are [www/js/app.js](https://bitbucket.org/CheckYourCarApp/checkyourcarapp/src/f45c6c20a1d74761c039c57d1b7e18a97400707a/CheckYourCar/www/js/app.js?at=master&fileviewer=file-view-default), [www/js/controllers.js](https://bitbucket.org/CheckYourCarApp/checkyourcarapp/src/f45c6c20a1d74761c039c57d1b7e18a97400707a/CheckYourCar/www/js/controllers.js?at=master&fileviewer=file-view-default) and [www/templates/list.html](https://bitbucket.org/CheckYourCarApp/checkyourcarapp/src/f45c6c20a1d74761c039c57d1b7e18a97400707a/CheckYourCar/www/templates/list.html?at=master&fileviewer=file-view-default)
* Tree data is described in JSON by id and parent's id in [www/js/data.js](https://bitbucket.org/CheckYourCarApp/checkyourcarapp/src/f45c6c20a1d74761c039c57d1b7e18a97400707a/CheckYourCar/www/js/data.js?at=master&fileviewer=file-view-default). 
* Other data printed in templates are icons, images, descriptions...
* Uses two types of card: list-avatar and card-facebook from [ionic templates](http://ionicframework.com/docs/components/)

# Author #

* Jordi Albert Viana Fons <jordivifo@gmail.com>

# License #

* MIT License (MIT). Copyright (c) 2016 Jordi Albert Viana Fons