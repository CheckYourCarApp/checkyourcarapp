
var starter = angular.module('starter.controllers', [])

starter.controller('AppCtrl', function ($scope) {
})
        .factory('ObjectFactory', function () {
            //Adds property childs
            var objects = $cards;
            for (var i = 0; i < objects.length; i++) {
                objects[i].childs = new Array();
                for (var j = 0; j < objects.length; j++) {
                    if (objects[j].pare == objects[i]._id) {
                        objects[i].childs.push(objects[j]._id);
                    }
                }
            }

            return {
                getById: function (id) {
                    if (!id)
                        return null;
                    for (var i in objects) {
                        if (objects[i]._id == id) {
                            return objects[i];
                        }
                    }
                    return null;
                },
                getParent: function (id) {
                    var o = this.getById(id);
                    if (!o)
                        return null;
                    return this.getById(o.pare)
                },
                getChilds: function (id) {
                    var a = [];
                    var o = this.getById(id);
                    if (!o || !o.childs)
                        return null
                    for (var i = 0; i < o.childs.length; i++) {
                        var t = o.childs[i]
                        a.push(this.getById(t));
                    }
                    return a;
                },
                getAll: function (){
                    var a = [];
                    for (var i = 0; i < objects.length; i++) {
                        a.push(objects[i]);
                    }
                    return a;
                }
            }
        })

starter.controller(':oName' + 'Ctrl', ['$scope', '$stateParams', 'ObjectFactory', function ($scope, $stateParams, ObjectFactory) {

        var o = ObjectFactory.getById($stateParams.oId);
        var ch = ObjectFactory.getChilds($stateParams.oId);

        $scope.oId = $stateParams.oId;
        $scope.oName = $stateParams.oName;
        $scope.obj = o;
        $scope.childs = ch;
        
    }])

starter.controller('BibCtrl', ['$scope', '$stateParams', 'ObjectFactory', function ($scope, $stateParams, ObjectFactory) {

        var list = ObjectFactory.getAll();
        $scope.list = list;
        
    }])
//