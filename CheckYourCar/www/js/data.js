var $cards = [
{
_id: 1,
        name: 'coche',
        pare: null,
        title: 'Coche',
        icon: null,
        image: null,
        text: null
}, {
_id: 112,
        name: 'gasolina',
        pare:1,
        title: 'Gasolina',
        icon: null,
        image: null,
        text: null,
        bib:null
},
{
_id: 2,
        name: 'diesel',
        pare: 1,
        title: 'Diesel',
        icon: null,
        image: null,
        text: null,
        bib:null
},
{
_id: 3,
        name: 'arranque',
        pare: 2,
        title: 'Arranque',
        icon: 'ion-ios-bolt-outline',
        image: null,
        text: null,
        bib:null
},
{
_id: 4,
        name: 'motor',
        pare: 2,
        title: 'Motor',
        icon: 'motor.png',
        image: null,
        text: null,
        bib:null
},
{
_id: 5,
        name: 'direccion',
        pare: 2,
        title: 'Dirección',
        icon: 'direccion.png',
        image: null,
        text: ['Entre lo más importante que siempre debemos tener como Dios manda en el coche, es el sistema de dirección. Si bien no es un sistema que sea demasiado complicado en funcionamiento y que cuente con muchos componentes, dichos componentes deben contar con el cuidado necesario para que el coche no nos dé una desagradable sorpresa mientras transitamos por carretera. Aunque parezca raro, lo básico que se debe tener en buen estado para que la dirección trabaje de manera efectiva son los amortiguadores. Los delanteros cargan todo el peso del coche hacia uno u otro lado en las curvas, de manera que si adelante todavía tenemos montados esos amortiguadores que compramos hace cinco años, poco harán para que evitemos una pérdida de control en una maniobra extrema o cuando transitemos sobre firme irregular. Pero además de los amortiguadores, hay factores mucho más sensibles que ponen en juego nuestra seguridad a la hora de mover el volante.La alineación del coche es otro tema al cual deberemos prestarle suma atención. Aqui también entra en juego el estado de los neumáticos, que se verán seriamente afectados si vamos con la dirección desalineada, de manera que si has llegado a extremos de transitar con el coche desalineado por mucho tiempo, tendrás que reemplazar también los neumáticos; los neumáticos de un coche desalineado pueden llegar a presentar un desgaste tan irregular en casos extremos, que podremos ver que uno de los lados de la banda de rodadura puede estar en las lonas, mientras que el otro lado está como nuevo… Además, con todos los baches en las calles y carreteras, el pan nuestro de cada día, la alineación de la dirección se va resintiendo poco a poco hasta que llega el momento en que podemos notar, simplemente con girar el volante, que la dirección no está correctamente alineada, o bien que el coche “tira” hacia un lado o hacia el otro cuando soltamos el volante yendo en línea recta (ojo que aqui también pueda deberse a una rueda más hinchada que la otra). De manera que los síntomas típicos de una dirección que no está correctamente alineada se harán notar y es difícil no advertirlos. Vale aclarar que después de hacer una reparación importante a cualquier componente de la dirección o de la suspensión delantera, el coche debe ser llevado al taller para alinear; es conveniente también hacerlo cuando compramos neumáticos nuevos, ya que todavía no se ha visto afectada su banda de rodadura por el defecto de pisado del coche.Recuerda que además de que la dirección es uno de los componentes en los que nos va, literalmente, la vida si sufrimos un accidente, por lo que su correcto mantenimiento y la alinación de las ruedas es uno de los mantenimientos que mejor relación dinero invertido/dinero ahorrado tiene de todos los que se hacen sobre el coche.'],
        bib: 'http://www.highmotor.com/direccion - coche - posibles - problemas.html'
},
{
_id: 6,
        name: 'calefaccion',
        pare: 2,
        title: 'Calefacción',
        icon: 'calefaccion.png',
        image: null,
        text: null,
        bib:null
},
{
_id: 7,
        name: 'probarrancar',
        pare: 3,
        title: 'Problemas al arrancar',
        icon: 'probarrancar.png',
        image: null,
        text: null,
        bib:null
},
{
_id: 8,
        name: 'noarranca',
        pare: 7,
        title: 'No arranca',
        icon: 'noarranca.png',
        image: null,
        text: ['<ul class="my-nav"> <li>Comprobación de fusibles: Algunos coches tienen un fusible asociado con el sistema de arranque, aunque antes de aventurarte a tocar donde no debes, - porque puede no resultar nada sencillo, comprueba que sabes lo que haces.</li> <li>Corrosión en la batería: Con el paso del tiempo las conexiones de la batería se pueden ensuciar o presentar muestras de corrosión. Dichos agentes pueden hacer que falle la conexión de la batería con el resto del sistema. Si es percepcible, trata de limpiar los bornes de la batería y vuelve a arrancar el coche de nuevo. No obstante, la corrosión puede afectar a más componentes del sistema de encendido, dificultándolo o impidiéndolo, pero será más difícil que puedas comprobarlos por ti mismo.</li> <li>La batería ha muerto: Aunque es la razón más habitual por la que tu coche no arrancará. ¿Tienes un comprobador de batería a mano? Si es así, mide el amperaje de la unidad: es probable que sea muy débil. Si dispones de cables de arranque - recuerda apagar el motor, y las luces del coche antes de hacer nada - puedes cerciorarte de que este es el problema con la ayuda de un automóvil y batería en buen estado. </li> <li>Fallo en el interruptor de encendido: Si la batería no es el problema, es probable que el origen del mismo esté en el propio contacto. Gira la llave a la posición de encendido (pero no todo). Si las luces de advertencia de color rojo del salpicadero no se encienden (y has comprobado que las conexiones de la batería están limpias), podemos pensar que, efectivamente, el interruptor de encendido falla.El interruptor de encendido genera una señal eléctrica al motor de arranque cuando accionas la llave. Es decir, pone en marcha el proceso que arranca el motor. Si no notas ningún ruido al girar la llave, es probable que se haya roto. </li> <li>Bujías sucias: Otro problema típico. Parece que el coche va a arrancar... pero no lo hace. Quizás las bujías estén sucias … o que directamente necesites cambiarlas. Algo que, en cualquier caso, te recomendamos que hagas en tu taller de confianza. </li>  </ul>'],
        bib: 'http://www.eurotaller.com/noticia/problemas - al - arrancar - el - coche - bateria - bujias - contacto - que - esta - fallando'
},
{
_id: 9,
        name: 'arrancamal',
        pare: 7,
        title: 'Arranca mal',
        icon: 'arrancamal.jpg',
        image: null,
        text: ['Las principales razones de que a su coche le cueste arrancar son las siguientes:', '<ul class="my-nav"> <li> Bujías de precalentamiento </li> <li> Resistencia de cables</li> <li>Presión de combustible </li>  <li>Tomas de aire suelta o dañadas  </li>  <li>Inyectores sucion que no abren </li>  <li> Sonda lambda del catalizador</li>  <li>Caudalímetro defectuoso </li>  <li>Potenciómetro de la mariposa </li>  <li> Sensor de picado de biela</li> <li> Sensor MAP</li><li> ECU </li> </ul>'],
        bib: 'http://www.pruebas.pieldetoro.net/web/bricos/bricosdetal.php?brico_id=3'
},
{
_id: 10,
        name: 'relenti',
        pare: 4,
        title: 'Relentí inestable',
        icon: 'relenti.png',
        image: null,
        text: ['Se denomina ralentí al estado de motor encendido sin carga y sin aceleración. El ralentí tiene por misión mantener listo el motor para su uso a pleno, en este estado el consumo de combustible es el menor posible y con energía disponible solamente para mantener el motor encendido y caliente mientras que la generación eléctrica es la necesaria y sufiencte para mantaner a plena carga la batería del automóvil. El ralentí no propociona potencia motriz útil, por lo que si se intenta mover el automóvil sin acelerar el motor se apagará. En modo ralentí el motor va tomando temperatura, por lo que las revoluciones del estado ralentí varían desde el arranque en frío (alrededor de 1100RPM a 1200RPM) hasta el estado estable, y una vez que se alcanza la temperatura de operación de motor - alrededor de 82 grados Celsius, las revoluciones se mantinen en un valor de 950RPM. Este proceso toma alrededor de 5 minutos. El proceso de ralentí se ajusta desde la Unidad de contro, Electrónico del automóvil (ECM) en base a los parámetros tomados de varios sensores: sensor de temperatura de aire, sensor de temperatura de refrigerante de motor, presión de aire. Cuando las revoluciones presentan variaciones anormales, de subida y bajada repentinamente, e incluso el motor trata de apagarse o se apaga por completo debemos buscar la causa en uno de los tres subsistemas siguientes:', '<ul class="my-nav"> <li>Sistema de ignición</li> <li>Sistema de combustible </li>  <li> Presión de compresión de las cámaras de combustión</li> </ul>', 'Estos tres susbsistemas afectan al motor produciendo una serie de fallas típicas:', ' <li>Pérdida o disminución de potencia del motor</li> <li>Inestabilidad en las revoluciones de ralentí</li> <li>Inestabilidad en las revoluciones en marcha</li> <li>Parada repentina del motor</li> <li>Dificultades a la hora de arranque en frío/caliente</li> <li>Temblores al aceleración o desacelerar</li> <li>Incrementos repentinos de potencia del motor con velocidad fija</li> <li>Bajo rendimiento de combustible probables causas: tiempo de ignición incorrecto, bujías de precalentamiento defectuosas: exceso de carbonilla o inservibles.</li>'],
        bib: 'http://autodaewoospark.com/ralenti - motor - inestable.php'
},
{
_id: 11,
        name: 'humonegro',
        pare: 4,
        title: 'Humo negro',
        icon: 'humonegro.png',
        image: null,
        text: ['En el caso de que el humo sea negro, esto es señal inequívoca de que el motor está quemando mucho combustible. Esto se produce por alguna posible deficiencia en el filtro de aire del motor, sensores de la inyección, inyectores y regulador de presión de combustible. El humo negro es generalmente la forma más fácil de diagnosticar un problema. Hágalo rápidamente porque tanto la salud de su coche como su propia situación económica lo agradecerán, ya que su vehículo dejará de gastar más cantidad del combustible estrictamente necesario.'],
        bib: 'http://www.autopista.es/trucos - y - consejos/todos - los - trucos - y - consejos/articulo/colores - humo - tubo - escape - averias - significado'
},
{
_id: 12,
        name: 'humoblanco',
        pare: 4,
        title: 'Humo blanco',
        icon: 'humoblanco.jpg',
        image: null,
        text: ['Si el humo que sale por el tubo de escape es de color blanco, en el caso de que salga en muy pequeñas cantidades y como con forma de hilos delgados, en principio no hay motivo de preocupación, ya que probablemente es el resultado de la acumulación de condensación normal en el interior del sistema de escape. Cuando el vehículo continúa acumulando kilómetros y después de unos minutos de funcionamiento, este fino humo blanco suele desaparecer. Sin embargo, cuando el hilo de humo blanco es más grueso y denso de lo normal, el problema es más grave, ya que lo que se está quemando es el líquido refrigerante del motor. Esto se puede producir por un grave problema en la junta de la culata, en la culata o una posible grieta en el bloque motor. El arreglo de todas averías suele llevar un gran desembolso económico. No ignores esto último, porque el problema podría agravarse. Una pequeña fuga de líquido refrigerante puede conducir a un riesgo serio de sobrecalentamiento y daños gravísimos en el motor. Asimismo, esa fuga de refrigerante también se puede mezclar con el aceite y causar serios problemas para su coche.', 'Los expertos en mecánica anuncian que cuando el humo que sale por el tubo de escape es de color gris, el problema es más difícil de diagnosticar. Al igual que cuando sale humo azul, con la aparición de humo gris el coche puede estar quemando aceite o montar un turbocompresor defectuoso. En este caso, las precauciones a seguir son las mismas que cuando sale humo azul. Además, el humo gris podría significar un atasco o mal funcionamiento del sistema PCV (sistema de ventilación positiva del cárter). Sin embargo, cuando la válvula de PCV se atasca, la presión puede generar fugas de aceite. Afortunadamente, las válvulas PCV no son caras y el problema puede ser solventado sencillamente por cualquier mecánico.' ],
        bib: 'http://www.autopista.es/trucos - y - consejos/todos - los - trucos - y - consejos/articulo/colores - humo - tubo - escape - averias - significado'
},
{
_id: 13,
        name: 'testigos',
        pare: 4,
        title: 'Testigos de averías encendidos',
        icon: 'testigos.png',
        image: null,
        text: [' <li>Testigo de fallo de batería: la batería o el sistema eléctrico presenta un fallo. Es necesario acudir al taller.</li> <li>Testigo de cinturón: Es necesario abrocharse el cinturón de seguridad.</li> <li>Testigo de fallo de la dirección asistida: la dirección no presenta asistencia, necesario acudir al taller.</li> <li>Testigo de freno de mano puesto. También puede indicarnos si se enciende en marcha de un fallo en el sistema de frenado. Hay que ir al taller.</li> <li>Presión o nivel de aceite: el nivel de aceite es bajo o la presión de aceite no es la suficiente. Revisar el nivel ya que podemos romper el motor.</li> <li>Testigo de control de tracción/estabilidad: si está encendido, el control de tracción/estabilidad esta desconectado pero si parpadea, es que está funcionando ya que detecta una pérdida de tracción.</li> <li>Testigo precalentamiento diésel: encendido indica que está funcionando el sistema de precalentamiento del combustible diésel. Hay que esperar a que se apague para poner en marcha el motor. Si parpadea, fallo en el sistema de inyección o encendido. Es necesario ir al taller.</li> <li>Testigo de pastillas de freno desgastadas.</li> <li>Testigo de nivel de combustible bajo.</li> <li>Testigo de fallo del ABS: acudir al taller</li> <li>Testigo de airbag: falla o bien ha sido desconectado el sistema de airbags.</li> <li>Testigo de dirección asistida: la asistencia está limitada. Es necesario ir al taller.</li> <li>Testigo de alumbrado: una de las lámparas exteriores se ha fundido.</li> <li>Fallo de motor: necesario ir al taller.</li> <li>Aceite o presión de aceite bajo: comprobar el nivel.</li> <li>Testigo líquido limpiaparabrisas: es necesario rellenarlo.</li> <li>Testigo de presión de inflado de ruedas: una de las ruedas presenta una presión insuficiente o bien existe un fallo en el sistema.'],
        bib: 'http://www.motorpasion.com/espaciotoyota/desciframos - todos - los - testigos - del - cuadro - de - instrumentos - mas - comunes'
},
{
_id: 14,
        name: 'motahoga',
        pare: 4,
        title: 'Motor se ahoga',
        icon: 'motor.png',
        image: null,
        text: [' Esto sucede cuando la mezcla de aire y combustible no se quema dentro de la cámara de combustión. Esto puede suceder por varias razones que van a requerir una intervención de un experto. Otra de las grandes razones son los problemas que se presentan en el flujo de aire. En dichos escenarios el flujo de aire en el motor no es suficiente y este no puede trabajar con total normalidad. Pero si te gusta la mecánica que decimos qué es lo que debes hacer y cuáles partes revisar para conocer la razón por la que se ahoga tu vehículo: ', '<ul class="my-nav"> <li>Verificar tubos en vacío.</li>  <li>Revisar la válvula control de aire al ralentí.</li> <li>Conocer el estado de la válvula de recirculación de gases.</li> <li>Problemas de transmisión.</li> <li>El suministro de combustible presenta fallas.</li> <li>Usar el scanner para revisar sensores eléctricos.</li> </ul>' ],
        bib: 'http://autos.starmedia.com/taller - mecanico/que - hacer - si - auto - ahoga.html '
},
{
_id: 15,
        name: 'faltapotencia',
        pare: 4,
        title: 'Falta de potencia',
        icon: 'motor.png',
        image: null,
        text: ['1. Frenos agarrotados: alguna pinza puede tener un pistón que no retrocede y la pastilla sigue actuando sobre el disco. Esta pérdida de potencia va a venir acompañada de un incremento en el consumo de combustible.', '2. Freno de mano muy tensado: es muy similar al anterior, es posible que lo lleves tan tensado que estés metiendo un rozamiento extra en el eje trasero.', '3. Rodamientos de las ruedas: es un elemento que permite que las ruedas puedan moverse libremente. Si el cojinete está mal, les costará moverse. Puedes notarlo porque harán mucho ruido.', '4.Un embrague muy tensado: esto solo suele ocurrir en los coches antiguos que no están dotados de embrague hidráulico. Lo notarás porque al subir una cuesta el coche se acelerará solo. Es posible que con un tensado soluciones es problema pero también puede estar gastado y te tocará cambiarlo.', '5. Comprueba el avance: si el avance no es el correcto, el coche fallará a bajas vueltas y en marchas largas.', '6. Bujías de precalentamiento en mal estado: estos son los 10 síntomas en las bujías para detectar averías. Léelos con detenimiento y sustitúyelas.', '7. Válvula pisada: es decir, que la válvula no cierra completamente. Puede ser que en frío funcione muy bien pero en caliente aparezca este fallo. Con un simple ajuste de válvulas se solucionaría este problema.', '8. Muelle de válvulas roto: el muelle de válvulas es el que lo empuje para que el sellado del cilindro sea perfecto. Producen un ruido característico en la culata muy fácil de detectar.', '9. Juego en las guías de válvulas: la guía de válvulas es el carril que, nunca mejor dicho, la guía. Si no es perfecto, no cerrarán bien el cilindro', '10. Árbol de levas gastado: el árbol de levas es el que empuja a las válvulas, si el coche tiene muchos kilómetros, es posible que esté gastado y por eso no cierran a la perfección.', '11. Filtro de aire muy sucio: el filtro de aire siempre debe estar limpio y libre de impurezas. Recuerda limpiarlo o sustituirlo.', '12. Entra aire extra a la admisión: si algún manguito o junta no está en buen estado, puede entrar aire extra al sistema de admisión. Así que la mezcla no será la ideal y la explosión en los cilindros no será perfecta.', '13. Pérdidas de presión: si el circuito de alimentación de combustible tiene alguna fuga, el caudal que llegará al motor no será el ideal. Además, una pérdida de gasolina siempre resulta muy peligrosa. ¡Riesgo de incendio! Asegúrate que el filtro de gasolina está limpio. Su está muy sucio, puede que no deje llegar el combustible con la suficiente presión y de ahí la pérdida de potencia.', '14. Tensión eléctrica: el combustible se envía a los cilindros por medio de una bomba, si no llega la suficiente tensión para darle energía a la bomba, esta no trabajará al 100% y la mezcla no será perfecta', '15. Caudalímetro: se encarga de medir la cantidad de aire que debe entrar en los cilindros. Está justo detrás del filtro de aire y es uno de los elementos más importantes de tu coche.', '16. Problemas en los inyectores: si están sucios, no van a introducir en los cilindros la cantidad ideal.', '17. Fallo en la centralita: este elemento electrónico controla que todo funcione según unos parámetros ajustado en fábrica. No deja de ser una placa base parecida a la de un ordenador que puede fallar, entre otras cosas, por un exceso de humedad en sus conexiones.' ],
        bib: 'http://www.autobild.es/reportajes/las - 20 - causas - perdida - potencia - tu - coche - 281151'
},
{
_id: 18,
        name: 'calefacción',
        pare: 6,
        title: 'Aire acondicionado',
        icon: 'calefaccion.png',
        image: null,
        text: [' Los sistemas de aire acondicionado que equipan los vehículos están detrás del 35% de averías que registran los talleres durante el verano.Algunos de los problemas más habituales que nos podemos encontrar son los fallos ocasionados en el compresor o alguna fuga en los conductos que transportan el gas. También se puede estropear el sistema de aire acondicionado de nuestro coche si no realizamos el mantenimiento adecuado. En este sentido, lo más habitual son las fugas de gas y aceite del compresor que, en caso de resultar dañado, sustituirlo por uno nuevo nos puede costar alrededor de 1.000 euros., Si la fuga del gas la tenemos por los conductos, que se vuelven porosos con el paso del tiempo, sustituirlos puede tener un precio aproximado de 200 o 300 euros. Los pequeños golpes que tenemos en la ciudad, o mismamente a la hora de aparcar el coche (hay quien aparca sin pensar en los coches de los demás), pueden dañar el sistema de aire acondicionado, entre otros elementos. Un golpe en nuestro coche puede dañar el condensador, que está situado en la parte delantera, junto a la defensa. Pero eso no es todo, ya que el filtro del habitáculo también se puede obstruir y ensuciar de tierra o insectos, debiéndolo sustituir cuando se encuentre en mal estado. Por lo tanto, es muy importante tener todos los elementos del sistema de aire acondicionado en buen estado. De esta forma podemos disfrutar de un ambiente agradable este verano en el interior de nuestro coche.'],
        bib: 'http://www.actualidadmotor.com/problemas - en - el - aire - acondicionado/'
},
{
_id: 24,
        name: 'arranque',
        pare: 112,
        title: 'Arranque',
        icon: 'ion-ios-bolt-outline',
        image: null,
        text: null,
        bib:null
},
{
_id: 25,
        name: 'motor',
        pare: 112,
        title: 'Motor',
        icon: 'motor.png',
        image: null,
        text: null,
        bib:null
},
{
_id: 26,
        name: 'direccion',
        pare: 112,
        title: 'Dirección',
        icon: 'direccion.png',
        image: null,
        text: ['Entre lo más importante que siempre debemos tener como Dios manda en el coche, es el sistema de dirección. Si bien no es un sistema que sea demasiado complicado en funcionamiento y que cuente con muchos componentes, dichos componentes deben contar con el cuidado necesario para que el coche no nos dé una desagradable sorpresa mientras transitamos por carretera. Aunque parezca raro, lo básico que se debe tener en buen estado para que la dirección trabaje de manera efectiva son los amortiguadores. Los delanteros cargan todo el peso del coche hacia uno u otro lado en las curvas, de manera que si adelante todavía tenemos montados esos amortiguadores que compramos hace cinco años, poco harán para que evitemos una pérdida de control en una maniobra extrema o cuando transitemos sobre firme irregular. Pero además de los amortiguadores, hay factores mucho más sensibles que ponen en juego nuestra seguridad a la hora de mover el volante.La alineación del coche es otro tema al cual deberemos prestarle suma atención. Aqui también entra en juego el estado de los neumáticos, que se verán seriamente afectados si vamos con la dirección desalineada, de manera que si has llegado a extremos de transitar con el coche desalineado por mucho tiempo, tendrás que reemplazar también los neumáticos; los neumáticos de un coche desalineado pueden llegar a presentar un desgaste tan irregular en casos extremos, que podremos ver que uno de los lados de la banda de rodadura puede estar en las lonas, mientras que el otro lado está como nuevo… Además, con todos los baches en las calles y carreteras, el pan nuestro de cada día, la alineación de la dirección se va resintiendo poco a poco hasta que llega el momento en que podemos notar, simplemente con girar el volante, que la dirección no está correctamente alineada, o bien que el coche “tira” hacia un lado o hacia el otro cuando soltamos el volante yendo en línea recta (ojo que aqui también pueda deberse a una rueda más hinchada que la otra). De manera que los síntomas típicos de una dirección que no está correctamente alineada se harán notar y es difícil no advertirlos. Vale aclarar que después de hacer una reparación importante a cualquier componente de la dirección o de la suspensión delantera, el coche debe ser llevado al taller para alinear; es conveniente también hacerlo cuando compramos neumáticos nuevos, ya que todavía no se ha visto afectada su banda de rodadura por el defecto de pisado del coche.Recuerda que además de que la dirección es uno de los componentes en los que nos va, literalmente, la vida si sufrimos un accidente, por lo que su correcto mantenimiento y la alinación de las ruedas es uno de los mantenimientos que mejor relación dinero invertido/dinero ahorrado tiene de todos los que se hacen sobre el coche.'],
        bib: 'http://www.highmotor.com/direccion - coche - posibles - problemas.html'
},
{
_id: 27,
        name: 'calefaccion',
        pare: 112,
        title: 'Calefacción',
        icon: 'calefaccion.png',
        image: null,
        text: null,
        bib:null
},
{
_id: 28,
        name: 'probarrancar',
        pare: 24,
        title: 'Problemas al arrancar',
        icon: 'probarrancar.png',
        image: null,
        text: null,
        bib:null
},
{
_id: 29,
        name: 'noarranca',
        pare: 28,
        title: 'No arranca',
        icon: 'noarranca.png',
        image: null,
        text: ['<ul class="my-nav"> <li>Comprobación de fusibles: Algunos coches tienen un fusible asociado con el sistema de arranque, aunque antes de aventurarte a tocar donde no debes, - porque puede no resultar nada sencillo, comprueba que sabes lo que haces.</li> <li>Corrosión en la batería: Con el paso del tiempo las conexiones de la batería se pueden ensuciar o presentar muestras de corrosión. Dichos agentes pueden hacer que falle la conexión de la batería con el resto del sistema. Si es percepcible, trata de limpiar los bornes de la batería y vuelve a arrancar el coche de nuevo. No obstante, la corrosión puede afectar a más componentes del sistema de encendido, dificultándolo o impidiéndolo, pero será más difícil que puedas comprobarlos por ti mismo.</li> <li>La batería ha muerto: Aunque es la razón más habitual por la que tu coche no arrancará. ¿Tienes un comprobador de batería a mano? Si es así, mide el amperaje de la unidad: es probable que sea muy débil. Si dispones de cables de arranque - recuerda apagar el motor, y las luces del coche antes de hacer nada - puedes cerciorarte de que este es el problema con la ayuda de un automóvil y batería en buen estado. </li> <li>Fallo en el interruptor de encendido: Si la batería no es el problema, es probable que el origen del mismo esté en el propio contacto. Gira la llave a la posición de encendido (pero no todo). Si las luces de advertencia de color rojo del salpicadero no se encienden (y has comprobado que las conexiones de la batería están limpias), podemos pensar que, efectivamente, el interruptor de encendido falla.El interruptor de encendido genera una señal eléctrica al motor de arranque cuando accionas la llave. Es decir, pone en marcha el proceso que arranca el motor. Si no notas ningún ruido al girar la llave, es probable que se haya roto. </li> <li>Bujías sucias: Otro problema típico. Parece que el coche va a arrancar... pero no lo hace. Quizás las bujías estén sucias … o que directamente necesites cambiarlas. Algo que, en cualquier caso, te recomendamos que hagas en tu taller de confianza. </li>  </ul>'],
        bib: 'http://www.eurotaller.com/noticia/problemas - al - arrancar - el - coche - bateria - bujias - contacto - que - esta - fallando'
},
{
_id: 30,
        name: 'arrancamal',
        pare: 28,
        title: 'Arranca mal',
        icon: 'arrancamal.jpg',
        image: null,
        text: ['Las principales razones de que a su coche le cueste arrancar son las siguientes:',
                '<ul class="my-nav"> <li> Bujías </li> <li> Resistencia de cables</li> <li>Presión de combustible </li>  <li>Tomas de aire suelta o dañadas  </li>  <li>Inyectores sucion que no abren </li>  <li> Sonda lambda del catalizador</li>  <li>Caudalímetro defectuoso </li>  <li>Potenciómetro de la mariposa </li>  <li> Sensor de picado de biela</li> <li> Sensor MAP</li><li> ECU </li> </ul>'],
        bib: 'http://www.pruebas.pieldetoro.net/web/bricos/bricosdetal.php?brico_id=3'
},
{
_id: 31,
        name: 'relenti',
        pare: 25,
        title: 'Relentí inestable',
        icon: 'relenti.png',
        image: null,
        text: ['Se denomina ralentí al estado de motor encendido sin carga y sin aceleración. El ralentí tiene por misión mantener listo el motor para su uso a pleno, en este estado el consumo de combustible es el menor posible y con energía disponible solamente para mantener el motor encendido y caliente mientras que la generación eléctrica es la necesaria y sufiencte para mantaner a plena carga la batería del automóvil. El ralentí no propociona potencia motriz útil, por lo que si se intenta mover el automóvil sin acelerar el motor se apagará. En modo ralentí el motor va tomando temperatura, por lo que las revoluciones del estado ralentí varían desde el arranque en frío (alrededor de 1100RPM a 1200RPM) hasta el estado estable, y una vez que se alcanza la temperatura de operación de motor - alrededor de 82 grados Celsius, las revoluciones se mantinen en un valor de 950RPM. Este proceso toma alrededor de 5 minutos. El proceso de ralentí se ajusta desde la Unidad de contro, Electrónico del automóvil (ECM) en base a los parámetros tomados de varios sensores: sensor de temperatura de aire, sensor de temperatura de refrigerante de motor, presión de aire. Cuando las revoluciones presentan variaciones anormales, de subida y bajada repentinamente, e incluso el motor trata de apagarse o se apaga por completo debemos buscar la causa en uno de los tres subsistemas siguientes:', '<ul class="my-nav"> <li>Sistema de ignición</li> <li>Sistema de combustible </li>  <li> Presión de compresión de las cámaras de combustión</li> </ul>', 'Estos tres susbsistemas afectan al motor produciendo una serie de fallas típicas:', ' <li>Pérdida o disminución de potencia del motor</li> <li>Inestabilidad en las revoluciones de ralentí</li> <li>Inestabilidad en las revoluciones en marcha</li> <li>Parada repentina del motor</li> <li>Dificultades a la hora de arranque en frío/caliente</li> <li>Temblores al aceleración o desacelerar</li> <li>Incrementos repentinos de potencia del motor con velocidad fija</li> <li>Bajo rendimiento de combustible probables causas: tiempo de ignición incorrecto, bujías defectuosas: exceso de carbón en electrodos, espaciado incorrecto, electrodos quemados.</li>'],
        bib: 'http://autodaewoospark.com/ralenti - motor - inestable.php'
},
{
_id: 32,
        name: 'humonegro',
        pare: 25,
        title: 'Humo negro',
        icon: 'humonegro.png',
        image: null,
        text: ['En el caso de que el humo sea negro, esto es señal inequívoca de que el motor está quemando mucho combustible. Esto se produce por alguna posible deficiencia en el filtro de aire del motor, sensores de la inyección, inyectores y regulador de presión de combustible. El humo negro es generalmente la forma más fácil de diagnosticar un problema. Hágalo rápidamente porque tanto la salud de su coche como su propia situación económica lo agradecerán, ya que su vehículo dejará de gastar más cantidad del combustible estrictamente necesario.'],
        bib: 'http://www.autopista.es/trucos - y - consejos/todos - los - trucos - y - consejos/articulo/colores - humo - tubo - escape - averias - significado'
},
{
_id: 33,
        name: 'humoblanco',
        pare: 25,
        title: 'Humo blanco',
        icon: 'humoblanco.jpg',
        image: null,
        text: ['Si el humo que sale por el tubo de escape es de color blanco, en el caso de que salga en muy pequeñas cantidades y como con forma de hilos delgados, en principio no hay motivo de preocupación, ya que probablemente es el resultado de la acumulación de condensación normal en el interior del sistema de escape. Cuando el vehículo continúa acumulando kilómetros y después de unos minutos de funcionamiento, este fino humo blanco suele desaparecer. Sin embargo, cuando el hilo de humo blanco es más grueso y denso de lo normal, el problema es más grave, ya que lo que se está quemando es el líquido refrigerante del motor. Esto se puede producir por un grave problema en la junta de la culata, en la culata o una posible grieta en el bloque motor. El arreglo de todas averías suele llevar un gran desembolso económico. No ignores esto último, porque el problema podría agravarse. Una pequeña fuga de líquido refrigerante puede conducir a un riesgo serio de sobrecalentamiento y daños gravísimos en el motor. Asimismo, esa fuga de refrigerante también se puede mezclar con el aceite y causar serios problemas para su coche.', 'Los expertos en mecánica anuncian que cuando el humo que sale por el tubo de escape es de color gris, el problema es más difícil de diagnosticar. Al igual que cuando sale humo azul, con la aparición de humo gris el coche puede estar quemando aceite o montar un turbocompresor defectuoso. En este caso, las precauciones a seguir son las mismas que cuando sale humo azul. Además, el humo gris podría significar un atasco o mal funcionamiento del sistema PCV (sistema de ventilación positiva del cárter). Sin embargo, cuando la válvula de PCV se atasca, la presión puede generar fugas de aceite. Afortunadamente, las válvulas PCV no son caras y el problema puede ser solventado sencillamente por cualquier mecánico.' ],
        bib: 'http://www.autopista.es/trucos - y - consejos/todos - los - trucos - y - consejos/articulo/colores - humo - tubo - escape - averias - significado'
},
{
_id: 34,
        name: 'testigos',
        pare: 25,
        title: 'Testigos de averías encendidos',
        icon: 'testigos.png',
        image: null,
        text: [' <li>Testigo de fallo de batería: la batería o el sistema eléctrico presenta un fallo. Es necesario acudir al taller.</li> <li>Testigo de cinturón: Es necesario abrocharse el cinturón de seguridad.</li> <li>Testigo de fallo de la dirección asistida: la dirección no presenta asistencia, necesario acudir al taller.</li> <li>Testigo de freno de mano puesto. También puede indicarnos si se enciende en marcha de un fallo en el sistema de frenado. Hay que ir al taller.</li> <li>Presión o nivel de aceite: el nivel de aceite es bajo o la presión de aceite no es la suficiente. Revisar el nivel ya que podemos romper el motor.</li> <li>Testigo de control de tracción/estabilidad: si está encendido, el control de tracción/estabilidad esta desconectado pero si parpadea, es que está funcionando ya que detecta una pérdida de tracción.</li> <li>Testigo precalentamiento diésel: encendido indica que está funcionando el sistema de precalentamiento del combustible diésel. Hay que esperar a que se apague para poner en marcha el motor. Si parpadea, fallo en el sistema de inyección o encendido. Es necesario ir al taller.</li> <li>Testigo de pastillas de freno desgastadas.</li> <li>Testigo de nivel de combustible bajo.</li> <li>Testigo de fallo del ABS: acudir al taller</li> <li>Testigo de airbag: falla o bien ha sido desconectado el sistema de airbags.</li> <li>Testigo de dirección asistida: la asistencia está limitada. Es necesario ir al taller.</li> <li>Testigo de alumbrado: una de las lámparas exteriores se ha fundido.</li> <li>Fallo de motor: necesario ir al taller.</li> <li>Aceite o presión de aceite bajo: comprobar el nivel.</li> <li>Testigo líquido limpiaparabrisas: es necesario rellenarlo.</li> <li>Testigo de presión de inflado de ruedas: una de las ruedas presenta una presión insuficiente o bien existe un fallo en el sistema.'],
        bib: 'http://www.motorpasion.com/espaciotoyota/desciframos - todos - los - testigos - del - cuadro - de - instrumentos - mas - comunes'
},
{
_id: 35,
        name: 'motahoga',
        pare: 25,
        title: 'Motor se ahoga',
        icon: 'motor.png',
        image: null,
        text: [' Esto sucede cuando la mezcla de aire y combustible no se quema dentro de la cámara de combustión. Esto puede suceder por varias razones que van a requerir una intervención de un experto. Otra de las grandes razones son los problemas que se presentan en el flujo de aire. En dichos escenarios el flujo de aire en el motor no es suficiente y este no puede trabajar con total normalidad. Pero si te gusta la mecánica que decimos qué es lo que debes hacer y cuáles partes revisar para conocer la razón por la que se ahoga tu vehículo: ', '<ul class="my-nav"> <li>Verificar tubos en vacío.</li>  <li>Revisar la válvula control de aire al ralentí.</li> <li>Conocer el estado de la válvula de recirculación de gases.</li> <li>Problemas de transmisión.</li> <li>El suministro de combustible presenta fallas.</li> <li>Usar el scanner para revisar sensores eléctricos.</li> </ul>' ],
        bib: 'http://autos.starmedia.com/taller - mecanico/que - hacer - si - auto - ahoga.html '
},
{
_id: 36,
        name: 'faltapotencia',
        pare: 25,
        title: 'Falta de potencia',
        icon: 'motor.png',
        image: null,
        text: ['1. Frenos agarrotados: alguna pinza puede tener un pistón que no retrocede y la pastilla sigue actuando sobre el disco. Esta pérdida de potencia va a venir acompañada de un incremento en el consumo de combustible.', '2. Freno de mano muy tensado: es muy similar al anterior, es posible que lo lleves tan tensado que estés metiendo un rozamiento extra en el eje trasero.', '3. Rodamientos de las ruedas: es un elemento que permite que las ruedas puedan moverse libremente. Si el cojinete está mal, les costará moverse. Puedes notarlo porque harán mucho ruido.', '4.Un embrague muy tensado: esto solo suele ocurrir en los coches antiguos que no están dotados de embrague hidráulico. Lo notarás porque al subir una cuesta el coche se acelerará solo. Es posible que con un tensado soluciones es problema pero también puede estar gastado y te tocará cambiarlo.', '5. Comprueba el avance: si el avance no es el correcto, el coche fallará a bajas vueltas y en marchas largas.', '6. Bujías de precalentamiento en mal estado: estos son los 10 síntomas en las bujías para detectar averías. Léelos con detenimiento y sustitúyelas.', '7. Válvula pisada: es decir, que la válvula no cierra completamente. Puede ser que en frío funcione muy bien pero en caliente aparezca este fallo. Con un simple ajuste de válvulas se solucionaría este problema.', '8. Muelle de válvulas roto: el muelle de válvulas es el que lo empuje para que el sellado del cilindro sea perfecto. Producen un ruido característico en la culata muy fácil de detectar.', '9. Juego en las guías de válvulas: la guía de válvulas es el carril que, nunca mejor dicho, la guía. Si no es perfecto, no cerrarán bien el cilindro', '10. Árbol de levas gastado: el árbol de levas es el que empuja a las válvulas, si el coche tiene muchos kilómetros, es posible que esté gastado y por eso no cierran a la perfección.', '11. Filtro de aire muy sucio: el filtro de aire siempre debe estar limpio y libre de impurezas. Recuerda limpiarlo o sustituirlo.', '12. Entra aire extra a la admisión: si algún manguito o junta no está en buen estado, puede entrar aire extra al sistema de admisión. Así que la mezcla no será la ideal y la explosión en los cilindros no será perfecta.', '13. Pérdidas de presión: si el circuito de alimentación de combustible tiene alguna fuga, el caudal que llegará al motor no será el ideal. Además, una pérdida de gasolina siempre resulta muy peligrosa. ¡Riesgo de incendio! Asegúrate que el filtro de gasolina está limpio. Su está muy sucio, puede que no deje llegar el combustible con la suficiente presión y de ahí la pérdida de potencia.', '14. Tensión eléctrica: el combustible se envía a los cilindros por medio de una bomba, si no llega la suficiente tensión para darle energía a la bomba, esta no trabajará al 100% y la mezcla no será perfecta', '15. Caudalímetro: se encarga de medir la cantidad de aire que debe entrar en los cilindros. Está justo detrás del filtro de aire y es uno de los elementos más importantes de tu coche.', '16. Problemas en los inyectores: si están sucios, no van a introducir en los cilindros la cantidad ideal.', '17. Fallo en la centralita: este elemento electrónico controla que todo funcione según unos parámetros ajustado en fábrica. No deja de ser una placa base parecida a la de un ordenador que puede fallar, entre otras cosas, por un exceso de humedad en sus conexiones.' ],
        bib: 'http://www.autobild.es/reportajes/las - 20 - causas - perdida - potencia - tu - coche - 281151'
},
{
_id: 37,
        name: 'calefacción',
        pare: 27,
        title: 'Aire acondicionado',
        icon: 'calefaccion.png',
        image: null,
        text: [' Los sistemas de aire acondicionado que equipan los vehículos están detrás del 35% de averías que registran los talleres durante el verano.Algunos de los problemas más habituales que nos podemos encontrar son los fallos ocasionados en el compresor o alguna fuga en los conductos que transportan el gas. También se puede estropear el sistema de aire acondicionado de nuestro coche si no realizamos el mantenimiento adecuado. En este sentido, lo más habitual son las fugas de gas y aceite del compresor que, en caso de resultar dañado, sustituirlo por uno nuevo nos puede costar alrededor de 1.000 euros., Si la fuga del gas la tenemos por los conductos, que se vuelven porosos con el paso del tiempo, sustituirlos puede tener un precio aproximado de 200 o 300 euros. Los pequeños golpes que tenemos en la ciudad, o mismamente a la hora de aparcar el coche (hay quien aparca sin pensar en los coches de los demás), pueden dañar el sistema de aire acondicionado, entre otros elementos. Un golpe en nuestro coche puede dañar el condensador, que está situado en la parte delantera, junto a la defensa. Pero eso no es todo, ya que el filtro del habitáculo también se puede obstruir y ensuciar de tierra o insectos, debiéndolo sustituir cuando se encuentre en mal estado. Por lo tanto, es muy importante tener todos los elementos del sistema de aire acondicionado en buen estado. De esta forma podemos disfrutar de un ambiente agradable este verano en el interior de nuestro coche.'],
        bib: 'http://www.actualidadmotor.com/problemas - en - el - aire - acondicionado/'
}

]
