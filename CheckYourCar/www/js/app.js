// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
starter = angular.module('starter', ['ionic', 'starter.controllers'])

starter.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

starter.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider.state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })

    $stateProvider.state('app.first', {
        url: "/",
        views: {
            'menuContent': {
                templateUrl: "templates/first.html"
            }
        }
    })

    $stateProvider.state('app.info', {
        url: "/info",
        views: {
            'menuContent': {
                templateUrl: "templates/info.html"
            }
        }
    })
    
    $stateProvider.state('app.bib', {
        url: "/bib",
        views: {
            'menuContent': {
                templateUrl: "templates/bib.html",
                controller: "BibCtrl"
            }
        }
    })
    
    $stateProvider.state('app.' + ':oName', {
        url: "/:oId",
        views: {
            'menuContent': {
                templateUrl: "templates/list.html",
                controller: ':oName' + 'Ctrl'
            }
        }
    })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/');
});
